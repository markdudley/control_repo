node default {
  $role = lookup('role')
  if $role != false {
    include "role::${role}"
  } else {
    include 'role::undefined'
  }
}
